<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('trabajos', function (Blueprint $table) {
            $table->id();
            $table->foreignId('cliente_proveedor_id')->constrained('clientes_proveedores');
            $table->foreignId('marca_id')->constrained('marcas');
            $table->foreignId('submarca_id')->constrained('marcas');
            $table->foreignId('modelo_id')->constrained('modelos');
            $table->text('observaciones')->nullable();
            $table->enum('estado', ['PENDIENTE','CONCLUIDO']);
            $table->boolean('activo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('trabajos');
    }
};
