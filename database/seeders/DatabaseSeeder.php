<?php

namespace Database\Seeders;

use App\Models\Medida;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        Schema::disableForeignKeyConstraints();

        $this->call([
            MarcasSeeder::class,
            MedidasSeeder::class,
            ModelosSeeder::class,
            ParteMotoresSeeder::class,
            ServiciosSeeder::class,
            TipoRefaccionesSeeder::class,
            UsersSeeder::class
        ]);

        Schema::disableForeignKeyConstraints();
    }
}
