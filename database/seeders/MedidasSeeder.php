<?php

namespace Database\Seeders;

use App\Models\Medida;
use Illuminate\Database\Seeder;

class MedidasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Medida::truncate();

        Medida::create([
            'nombre' => '010',
            'activo' => true
        ]);

        Medida::create([
            'nombre' => '020',
            'activo' => true
        ]);

        Medida::create([
            'nombre' => '020',
            'activo' => true
        ]);

        Medida::create([
            'nombre' => '040',
            'activo' => true
        ]);

        Medida::create([
            'nombre' => 'STD',
            'activo' => true
        ]);
    }
}
