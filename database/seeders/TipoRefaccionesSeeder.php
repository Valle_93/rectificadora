<?php

namespace Database\Seeders;

use App\Models\{ParteMotor,TipoRefaccion};
use Illuminate\Database\Seeder;

class TipoRefaccionesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $columnas  = ['id'];
        $cabeza    = ParteMotor::where('nombre','Cabeza de motor')->first($columnas);
        $monoblock = ParteMotor::where('nombre','Monoblock')->first($columnas);
        $piston    = ParteMotor::where('nombre','Pistones')->first($columnas);

        TipoRefaccion::truncate();

        TipoRefaccion::create([
            'nombre'         => 'Válvulas de admisión',
            'parte_motor_id' => $cabeza->id,
            'activo'         => true
        ]);

        TipoRefaccion::create([
            'nombre'         => 'Válvulas de escape',
            'parte_motor_id' => $cabeza->id,
            'activo'         => true
        ]);

        TipoRefaccion::create([
            'nombre'         => 'Sellos de válvula',
            'parte_motor_id' => $cabeza->id,
            'activo'         => true
        ]);

        TipoRefaccion::create([
            'nombre'         => 'Guias',
            'parte_motor_id' => $cabeza->id,
            'activo'         => true
        ]);

        TipoRefaccion::create([
            'nombre'         => 'Anillos',
            'parte_motor_id' => $piston->id,
            'activo'         => true
        ]);

        TipoRefaccion::create([
            'nombre'         => 'Metales de biela',
            'parte_motor_id' => $piston->id,
            'activo'         => true
        ]);

        TipoRefaccion::create([
            'nombre'         => 'Metales de centro',
            'parte_motor_id' => $monoblock->id,
            'activo'         => true
        ]);
    }
}
