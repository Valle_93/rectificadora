<?php

namespace Database\Seeders;

use Carbon\Carbon;
use App\Models\Modelo;
use Illuminate\Database\Seeder;

class ModelosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Modelo::truncate();

        $hoy = Carbon::now()->year;

        for ($i = 1990; $i <= $hoy ; $i++) {
            Modelo::create([
                'anio'   => $i,
                'activo' => true
            ]);
        }
    }
}
