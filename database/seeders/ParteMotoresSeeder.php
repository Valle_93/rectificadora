<?php

namespace Database\Seeders;

use App\Models\ParteMotor;
use Illuminate\Database\Seeder;

class ParteMotoresSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        ParteMotor::truncate();

        ParteMotor::create([
            'nombre' => 'Cabeza de motor',
            'activo' => true
        ]);

        ParteMotor::create([
            'nombre' => 'Monoblock',
            'activo' => true
        ]);

        ParteMotor::create([
            'nombre' => 'Pistones',
            'activo' => true
        ]);

        ParteMotor::create([
            'nombre' => 'Cigüeñal',
            'activo' => true
        ]);
    }
}
