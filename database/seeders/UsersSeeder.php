<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        User::truncate();

        User::create([
            'name'     => 'Heriberto Garcia Valle',
            'email'    => 'valle9307@hotmail.com',
            'password' => Hash::make('admin123')
        ]);
    }
}
