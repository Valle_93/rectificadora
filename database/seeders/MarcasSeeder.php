<?php

namespace Database\Seeders;

use App\Models\Marca;
use Illuminate\Database\Seeder;

class MarcasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Marca::truncate();

        Marca::create([
            'nombre' => 'Volkswagen',
            'activo' => true
        ]);

        Marca::create([
            'nombre' => 'Nissan',
            'activo' => true
        ]);

        Marca::create([
            'nombre' => 'Honda',
            'activo' => true
        ]);

        Marca::create([
            'nombre' => 'Renault',
            'activo' => true
        ]);

        Marca::create([
            'nombre' => 'Ford',
            'activo' => true
        ]);

        Marca::create([
            'nombre' => 'Audi',
            'activo' => true
        ]);

        Marca::create([
            'nombre' => 'Mazda',
            'activo' => true
        ]);

        Marca::create([
            'nombre' => 'Acura',
            'activo' => true
        ]);

        Marca::create([
            'nombre' => 'Chevrolet',
            'activo' => true
        ]);

        Marca::create([
            'nombre' => 'Seat',
            'activo' => true
        ]);

        Marca::create([
            'nombre' => 'Toyota',
            'activo' => true
        ]);

        Marca::create([
            'nombre' => 'Mitsubishi',
            'activo' => true
        ]);

        Marca::create([
            'nombre' => 'Chryster',
            'activo' => true
        ]);

        Marca::create([
            'nombre' => 'Jeep',
            'activo' => true
        ]);

        Marca::create([
            'nombre' => 'BMW',
            'activo' => true
        ]);

        Marca::create([
            'nombre' => 'Volvo',
            'activo' => true
        ]);

        Marca::create([
            'nombre' => 'Mercedez Beanz',
            'activo' => true
        ]);

        Marca::create([
            'nombre' => 'Susuki',
            'activo' => true
        ]);
    }
}
