<?php

namespace Database\Factories;

use App\Models\{ParteMotor,Servicio};
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Servicio>
 */
class ServicioFactory extends Factory
{
     /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Servicio::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'nombre'         => $this->faker->sentence(5, true),
            'parte_motor_id' => ParteMotor::inRandomOrder()->first()->id,
            'activo'         => true
        ];
    }
}
