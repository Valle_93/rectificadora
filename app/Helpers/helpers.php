<?php

if (!function_exists('api_response')) {
    function api_response($data = [], $mensaje = 'ok', $code = 200)
    {
        $code = intval($code);

        return response()->json([
            'data'     => $data,
            'mensaje'  => $mensaje
        ], $code);
    }
}
