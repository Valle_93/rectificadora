<?php

namespace App\Http\Controllers\Api\Auth;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use App\Http\Requests\Auth\{LoginRequest,RegistroRequest};


class AuthController extends Controller
{
    public function login(LoginRequest $request)
    {
        $credenciales = $request->validated();

        if ($this->guard()->attempt($credenciales)) {
            $request->session()->regenerate();

            return api_response($this->guard()->user());
        }

        throw ValidationException::withMessages([
            'email' => [trans('auth.failed')]
        ]);
    }

    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return api_response();
    }

    public function registro(RegistroRequest $request)
    {
        $user = null;

        try {
            $data             = $request->validated();
            $data['password'] = Hash::make($data['password']);

            $user = User::create($data);

            $this->code    = 200;
            $this->mensaje = 'Registro realizaco correctamente';
        } catch (\Throwable $th) {
            $this->code    = 500;
            $this->mensaje = $th->getMessage();
        }

        return api_response($user, $this->mensaje, $this->code);
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
    */
    protected function guard()
    {
        return Auth::guard();
    }
}
