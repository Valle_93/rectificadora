<?php

namespace App\Http\Controllers\Api\Abonos;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\{Abono,Cuenta};
use App\Http\Requests\Abonos\AbonoRequest;

class AbonoController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(AbonoRequest $request)
    {
        try {
            $data = $request->validated();

            $abono = Abono::create($data);
            $abono->load('cuenta.abonos');

            $this->code    = 200;
            $this->mensaje = 'Abono creado correctamente';
        } catch (\Throwable $th) {
            $this->code    = 500;
            $this->mensaje = $th->getMessage();
        }

        return api_response([], $this->mensaje, $this->code);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(AbonoRequest $request, string $id)
    {
        try {
            $data  = $request->validated();
            $abono = Abono::with('cuenta.abonos')->findOrFail($id);

            $abono->update($data);

            $this->code    = 200;
            $this->mensaje = 'Abono actualizado correctamente';
        } catch (\Throwable $th) {
            $this->code    = 500;
            $this->mensaje = $th->getMessage();
        }

        return api_response([], $this->mensaje, $this->code);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        try {
            $abono = Abono::with('cuenta.abonos')->findOrFail($id);

            $abono->delete();

            $this->code    = 200;
            $this->mensaje = 'Abono eliminado correctamente';
        } catch (\Throwable $th) {
            $this->code    = 500;
            $this->mensaje = $th->getMessage();
        }

        return api_response([], $this->mensaje, $this->code);
    }
}
