<?php

namespace App\Http\Controllers\Api\Catalogos;

use App\Models\Modelo;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Catalogos\ModeloRequest;

class ModeloController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $data    = [];
        $perPage = intval($request->input('per_page'));
        $query   = $request->input('query');

        try {
            $data = Modelo::ofBusqueda($query)
            ->orderBy('anio')
            ->paginate($perPage);

            $this->code    = 200;
            $this->mensaje = '';
        } catch (\Throwable $th) {
            $this->code    = 500;
            $this->mensaje = $th->getMessage();
        }

        return api_response($data, $this->mensaje, $this->code);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(ModeloRequest $request)
    {
        try {
            $data = $request->validated();

            Modelo::create($data);

            $this->code    = 200;
            $this->mensaje = 'Modelo creado correctamente';
        } catch (\Throwable $th) {
            $this->code    = 500;
            $this->mensaje = $th->getMessage();
        }

        return api_response([], $this->mensaje, $this->code);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $data = [];

        try {
            $data = Modelo::findOrFail($id);

            $this->code    = 200;
            $this->mensaje = '';
        } catch (\Throwable $th) {
            $this->code    = 404;
            $this->mensaje = $th->getMessage();
        }

        return api_response($data, $this->mensaje, $this->code);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(ModeloRequest $request, string $id)
    {
        try {
            $data = $request->validated();

            $modelo = Modelo::findOrFail($id);
            $modelo->update($data);

            $this->code    = 200;
            $this->mensaje = 'Modelo actualizado correctamente';
        } catch (\Throwable $th) {
            $this->code    = 500;
            $this->mensaje = $th->getMessage();
        }

        return api_response([], $this->mensaje, $this->code);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        try {
            $modelo = Modelo::findOrFail($id);
            $modelo->update([
                'activo' => !$modelo->activo
            ]);

            $this->code    = 200;
            $this->mensaje = ($modelo->activo)
            ? 'Modelo desactivado correctamente'
            : 'Modelo activado correctamente';
        } catch (\Throwable $th) {
            $this->code    = 500;
            $this->mensaje = $th->getMessage();
        }

        return api_response([], $this->mensaje, $this->code);
    }

    public function listado()
    {
        $data  = [];

        try {
            $data = Modelo::where('activo', true)
            ->orderBy('anio')
            ->get();

            $this->code    = 200;
            $this->mensaje = '';
        } catch (\Throwable $th) {
            $this->code    = 500;
            $this->mensaje = $th->getMessage();
        }

        return api_response($data, $this->mensaje, $this->code);
    }
}
