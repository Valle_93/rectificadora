<?php

namespace App\Http\Controllers\Api\Catalogos;

use App\Models\ClienteProveedor;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Catalogos\ClienteProveedorRequest;

class ClienteProveedorController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $data    = [];
        $perPage = intval($request->input('per_page'));
        $query   = $request->input('query');

        try {
            $data = ClienteProveedor::ofBusqueda($query)
            ->orderBy('nombre')
            ->paginate($perPage);

            $this->code    = 200;
            $this->mensaje = '';
        } catch (\Throwable $th) {
            $this->code    = 500;
            $this->mensaje = $th->getMessage();
        }

        return api_response($data, $this->mensaje, $this->code);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(ClienteProveedorRequest $request)
    {
        try {
            $data = $request->validated();

            ClienteProveedor::create($data);

            $this->code    = 200;
            $this->mensaje = 'Cliente/Proveedor creado correctamente';
        } catch (\Throwable $th) {
            $this->code    = 500;
            $this->mensaje = $th->getMessage();
        }

        return api_response([], $this->mensaje, $this->code);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $data = [];

        try {
            $data = ClienteProveedor::findOrFail($id);

            $this->code    = 200;
            $this->mensaje = '';
        } catch (\Throwable $th) {
            $this->code    = 404;
            $this->mensaje = $th->getMessage();
        }

        return api_response($data, $this->mensaje, $this->code);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(ClienteProveedorRequest $request, string $id)
    {
        try {
            $data = $request->validated();

            $clienteProveedor = ClienteProveedor::findOrFail($id);
            $clienteProveedor->update($data);

            $this->code    = 200;
            $this->mensaje = 'Cliente\Proveedor actualizado correctamente';
        } catch (\Throwable $th) {
            $this->code    = 500;
            $this->mensaje = $th->getMessage();
        }

        return api_response([], $this->mensaje, $this->code);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        try {
            $clienteProveedor = ClienteProveedor::findOrFail($id);
            $clienteProveedor->update([
                'activo' => !$clienteProveedor->activo
            ]);

            $this->code    = 200;
            $this->mensaje = ($clienteProveedor->activo)
            ? 'Cliente/Proveedor desactivado correctamente'
            : 'Cliente/Proveedor activado correctamente';
        } catch (\Throwable $th) {
            $this->code    = 500;
            $this->mensaje = $th->getMessage();
        }

        return api_response([], $this->mensaje, $this->code);
    }

    public function listado(Request $request)
    {
        $data        = [];
        $clientes    = $request->boolean('clientes');
        $proveedores = $request->boolean('proveedores');
        try {
            $columnas = ['id','nombre','rfc'];

            $data = ClienteProveedor::when(
                $clientes,
                fn ($query) => $query->where('tipo', 'CLIENTE')
                    ->orWhere('tipo', 'AMBOS')
            )
            ->when(
                $proveedores,
                fn ($query) => $query->where('tipo', 'PROVEEDOR')
                    ->orWhere('tipo', 'AMBOS')
            )
            ->where('activo', true)
            ->orderBy('nombre')
            ->get($columnas);

            $this->mensaje = null;
            $this->code  = 200;
        } catch (\Throwable $th) {
            $this->code    = 500;
            $this->mensaje = $th->getMessage();
        }

        return api_response($data, $this->mensaje, $this->code);
    }
}
