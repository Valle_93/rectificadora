<?php

namespace App\Http\Controllers\Api\Catalogos;

use App\Models\ParteMotor;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Catalogos\ParteMotorRequest;

class ParteMotorController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $data    = [];
        $perPage = intval($request->input('per_page'));
        $query   = $request->input('query');

        try {
            $data = ParteMotor::ofBusqueda($query)
            ->orderBy('nombre')
            ->paginate($perPage);

            $this->code    = 200;
            $this->mensaje = '';
        } catch (\Throwable $th) {
            $this->code    = 500;
            $this->mensaje = $th->getMessage();
        }

        return api_response($data, $this->mensaje, $this->code);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(ParteMotorRequest $request)
    {
        try {
            $data = $request->validated();

            ParteMotor::create($data);

            $this->code    = 200;
            $this->mensaje = 'Parte de motor creada correctamente';
        } catch (\Throwable $th) {
            $this->code    = 500;
            $this->mensaje = $th->getMessage();
        }

        return api_response([], $this->mensaje, $this->code);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $data = [];

        try {
            $data = ParteMotor::findOrFail($id);

            $this->code    = 200;
            $this->mensaje = '';
        } catch (\Throwable $th) {
            $this->code    = 404;
            $this->mensaje = $th->getMessage();
        }

        return api_response($data, $this->mensaje, $this->code);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(ParteMotorRequest $request, string $id)
    {
        try {
            $data = $request->validated();

            $parteMotor = ParteMotor::findOrFail($id);
            $parteMotor->update($data);

            $this->code    = 200;
            $this->mensaje = 'Parte de motor actualizada correctamente';
        } catch (\Throwable $th) {
            $this->code    = 500;
            $this->mensaje = $th->getMessage();
        }

        return api_response([], $this->mensaje, $this->code);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        try {
            $parteMotor = ParteMotor::findOrFail($id);
            $parteMotor->update([
                'activo' => !$parteMotor->activo
            ]);

            $this->code    = 200;
            $this->mensaje = ($parteMotor->activo)
            ? 'Parte de motor desactivada correctamente'
            : 'Parte de motor activada correctamente';
        } catch (\Throwable $th) {
            $this->code    = 500;
            $this->mensaje = $th->getMessage();
        }

        return api_response([], $this->mensaje, $this->code);
    }

    public function listado()
    {
        $data    = [];

        try {
            $data = ParteMotor::where('activo', true)
            ->orderBy('nombre')
            ->get();

            $this->code    = 200;
            $this->mensaje = '';
        } catch (\Throwable $th) {
            $this->code    = 500;
            $this->mensaje = $th->getMessage();
        }

        return api_response($data, $this->mensaje, $this->code);
    }
}
