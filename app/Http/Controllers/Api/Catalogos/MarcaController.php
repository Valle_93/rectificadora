<?php

namespace App\Http\Controllers\Api\Catalogos;

use App\Models\Marca;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Catalogos\MarcaRequest;

class MarcaController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $data    = [];
        $perPage = intval($request->input('per_page'));
        $query   = $request->input('query');

        try {
            $data = Marca::ofBusqueda($query)
            ->with('marca:id,nombre')
            ->orderBy('nombre')
            ->paginate($perPage);

            $this->code    = 200;
            $this->mensaje = '';
        } catch (\Throwable $th) {
            $this->code    = 500;
            $this->mensaje = $th->getMessage();
        }

        return api_response($data, $this->mensaje, $this->code);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(MarcaRequest $request)
    {
        try {
            $data = $request->validated();

            Marca::create($data);

            $this->code    = 200;
            $this->mensaje = 'Marca creada correctamente';
        } catch (\Throwable $th) {
            $this->code    = 500;
            $this->mensaje = $th->getMessage();
        }

        return api_response([], $this->mensaje, $this->code);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $data = [];

        try {
            $data = Marca::findOrFail($id);

            $this->code    = 200;
            $this->mensaje = '';
        } catch (\Throwable $th) {
            $this->code    = 404;
            $this->mensaje = $th->getMessage();
        }

        return api_response($data, $this->mensaje, $this->code);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(MarcaRequest $request, string $id)
    {
        try {
            $data = $request->validated();

            $marca = Marca::findOrFail($id);
            $marca->update($data);

            $this->code    = 200;
            $this->mensaje = 'Marca actualizada correctamente';
        } catch (\Throwable $th) {
            $this->code    = 500;
            $this->mensaje = $th->getMessage();
        }

        return api_response([], $this->mensaje, $this->code);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        try {
            $marca = Marca::findOrFail($id);
            $marca->update([
                'activo' => !$marca->activo
            ]);

            $this->code    = 200;
            $this->mensaje = ($marca->activo)
            ? 'Marca desactivada correctamente'
            : 'Marca activada correctamente';
        } catch (\Throwable $th) {
            $this->code    = 500;
            $this->mensaje = $th->getMessage();
        }

        return api_response([], $this->mensaje, $this->code);
    }

    public function marcasPadre()
    {
        $data = [];

        try {
            $data = Marca::whereNull('marca_id')
                ->orderBy('nombre')
                ->get();

            $this->code    = 200;
            $this->mensaje = '';
        } catch (\Throwable $th) {
            $this->code    = 500;
            $this->mensaje = $th->getMessage();
        }

        return api_response($data, $this->mensaje, $this->code);
    }

    public function submarcas(string $marcaId)
    {
        $data = [];

        try {
            $data = Marca::where('marca_id', $marcaId)
                ->orderBy('nombre')
                ->get();

            $this->code    = 200;
            $this->mensaje = '';
        } catch (\Throwable $th) {
            $this->code    = 500;
            $this->mensaje = $th->getMessage();
        }

        return api_response($data, $this->mensaje, $this->code);
    }
}
