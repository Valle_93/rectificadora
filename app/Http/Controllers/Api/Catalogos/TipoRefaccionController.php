<?php

namespace App\Http\Controllers\Api\Catalogos;

use App\Models\TipoRefaccion;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Catalogos\TipoRefaccionRequest;

class TipoRefaccionController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $data    = [];
        $perPage = intval($request->input('per_page'));
        $query   = $request->input('query');

        try {
            $data = TipoRefaccion::ofBusqueda($query)
            ->with('parteMotor')
            ->orderBy('nombre')
            ->paginate($perPage);

            $this->code    = 200;
            $this->mensaje = '';
        } catch (\Throwable $th) {
            $this->code    = 500;
            $this->mensaje = $th->getMessage();
        }

        return api_response($data, $this->mensaje, $this->code);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(TipoRefaccionRequest $request)
    {
        try {
            $data = $request->validated();

            TipoRefaccion::create($data);

            $this->code    = 200;
            $this->mensaje = 'Tipo de refacción creado correctamente';
        } catch (\Throwable $th) {
            $this->code    = 500;
            $this->mensaje = $th->getMessage();
        }

        return api_response([], $this->mensaje, $this->code);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $data = [];

        try {
            $data = TipoRefaccion::findOrFail($id);

            $this->code    = 200;
            $this->mensaje = '';
        } catch (\Throwable $th) {
            $this->code    = 404;
            $this->mensaje = $th->getMessage();
        }

        return api_response($data, $this->mensaje, $this->code);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(TipoRefaccionRequest $request, string $id)
    {
        try {
            $data = $request->validated();

            $tipoRefaccion = TipoRefaccion::findOrFail($id);
            $tipoRefaccion->update($data);

            $this->code    = 200;
            $this->mensaje = 'Tipo de refacción actualizado correctamente';
        } catch (\Throwable $th) {
            $this->code    = 500;
            $this->mensaje = $th->getMessage();
        }

        return api_response([], $this->mensaje, $this->code);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        try {
            $tipoRefaccion = TipoRefaccion::findOrFail($id);
            $tipoRefaccion->update([
                'activo' => !$tipoRefaccion->activo
            ]);

            $this->code    = 200;
            $this->mensaje = ($tipoRefaccion->activo)
            ? 'Tipo de refacción desactivado correctamente'
            : 'Tipo de refacción activado correctamente';
        } catch (\Throwable $th) {
            $this->code    = 500;
            $this->mensaje = $th->getMessage();
        }

        return api_response([], $this->mensaje, $this->code);
    }
}
