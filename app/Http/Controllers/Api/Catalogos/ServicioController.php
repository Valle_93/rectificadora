<?php

namespace App\Http\Controllers\Api\Catalogos;

use App\Models\Servicio;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Catalogos\ServicioRequest;

class ServicioController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $data    = [];
        $perPage = intval($request->input('per_page'));
        $query   = $request->input('query');

        try {
            $data = Servicio::ofBusqueda($query)
            ->with('parteMotor')
            ->orderBy('nombre')
            ->paginate($perPage);

            $this->code    = 200;
            $this->mensaje = '';
        } catch (\Throwable $th) {
            $this->code    = 500;
            $this->mensaje = $th->getMessage();
        }

        return api_response($data, $this->mensaje, $this->code);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(ServicioRequest $request)
    {
        try {
            $data = $request->validated();

            Servicio::create($data);

            $this->code    = 200;
            $this->mensaje = 'Servicio creado correctamente';
        } catch (\Throwable $th) {
            $this->code    = 500;
            $this->mensaje = $th->getMessage();
        }

        return api_response([], $this->mensaje, $this->code);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $data = [];

        try {
            $data = Servicio::findOrFail($id);

            $this->code    = 200;
            $this->mensaje = '';
        } catch (\Throwable $th) {
            $this->code    = 404;
            $this->mensaje = $th->getMessage();
        }

        return api_response($data, $this->mensaje, $this->code);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(ServicioRequest $request, string $id)
    {
        try {
            $data = $request->validated();

            $servicio = Servicio::findOrFail($id);
            $servicio->update($data);

            $this->code    = 200;
            $this->mensaje = 'Servicio actualizado correctamente';
        } catch (\Throwable $th) {
            $this->code    = 500;
            $this->mensaje = $th->getMessage();
        }

        return api_response([], $this->mensaje, $this->code);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        try {
            $servicio = Servicio::findOrFail($id);
            $servicio->update([
                'activo' => !$servicio->activo
            ]);

            $this->code    = 200;
            $this->mensaje = ($servicio->activo)
            ? 'Servicio desactivado correctamente'
            : 'Servicio activado correctamente';
        } catch (\Throwable $th) {
            $this->code    = 500;
            $this->mensaje = $th->getMessage();
        }

        return api_response([], $this->mensaje, $this->code);
    }

    public function listado(Request $request)
    {
        $data         = [];
        $parteMotorId = $request->input('parte_motor_id');

        try {
            $data = Servicio::when(
                $parteMotorId,
                fn ($query) => $query->where('parte_motor_id', $parteMotorId)
            )
            ->with('parteMotor')
            ->orderBy('nombre')
            ->get();

            $this->code    = 200;
            $this->mensaje = '';
        } catch (\Throwable $th) {
            $this->code    = 500;
            $this->mensaje = $th->getMessage();
        }

        return api_response($data, $this->mensaje, $this->code);
    }
}
