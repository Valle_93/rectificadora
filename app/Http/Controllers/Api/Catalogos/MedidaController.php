<?php

namespace App\Http\Controllers\Api\Catalogos;

use App\Models\Medida;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Catalogos\MedidaRequest;

class MedidaController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $data    = [];
        $perPage = intval($request->input('per_page'));
        $query   = $request->input('query');

        try {
            $data = Medida::ofBusqueda($query)
            ->orderBy('nombre')
            ->paginate($perPage);

            $this->code    = 200;
            $this->mensaje = '';
        } catch (\Throwable $th) {
            $this->code    = 500;
            $this->mensaje = $th->getMessage();
        }

        return api_response($data, $this->mensaje, $this->code);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(MedidaRequest $request)
    {
        try {
            $data = $request->validated();

            Medida::create($data);

            $this->code    = 200;
            $this->mensaje = 'Medida creada correctamente';
        } catch (\Throwable $th) {
            $this->code    = 500;
            $this->mensaje = $th->getMessage();
        }

        return api_response([], $this->mensaje, $this->code);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $data = [];

        try {
            $data = Medida::findOrFail($id);

            $this->code    = 200;
            $this->mensaje = '';
        } catch (\Throwable $th) {
            $this->code    = 404;
            $this->mensaje = $th->getMessage();
        }

        return api_response($data, $this->mensaje, $this->code);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(MedidaRequest $request, string $id)
    {
        try {
            $data = $request->validated();

            $medida = Medida::findOrFail($id);
            $medida->update($data);

            $this->code    = 200;
            $this->mensaje = 'Medida actualizada correctamente';
        } catch (\Throwable $th) {
            $this->code    = 500;
            $this->mensaje = $th->getMessage();
        }

        return api_response([], $this->mensaje, $this->code);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        try {
            $medida = Medida::findOrFail($id);
            $medida->update([
                'activo' => !$medida->activo
            ]);

            $this->code    = 200;
            $this->mensaje = ($medida->activo)
            ? 'Medida desactivada correctamente'
            : 'Medida activada correctamente';
        } catch (\Throwable $th) {
            $this->code    = 500;
            $this->mensaje = $th->getMessage();
        }

        return api_response([], $this->mensaje, $this->code);
    }
}
