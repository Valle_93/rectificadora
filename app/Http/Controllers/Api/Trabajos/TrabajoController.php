<?php

namespace App\Http\Controllers\Api\Trabajos;

use App\Models\Trabajo;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Trabajos\TrabajoRequest;

class TrabajoController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $data    = [];
        $perPage = intval($request->input('per_page'));
        $query   = $request->input('query');

        try {
            $data = Trabajo::ofBusqueda($query)
            ->with([
                'clienteProveedor:id,nombre',
                'marca:id,nombre',
                'submarca:id,marca_id,nombre',
                'modelo:id,anio'
            ])
            ->withCount('servicios')
            ->orderBy('created_at')
            ->paginate($perPage);

            $this->code    = 200;
            $this->mensaje = '';
        } catch (\Throwable $th) {
            $this->code    = 500;
            $this->mensaje = $th->getMessage();
        }

        return api_response($data, $this->mensaje, $this->code);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(TrabajoRequest $request)
    {
        try {
            $data             = $request->validated();
            $serviciosRequest = $request->input('servicios');
            $servicios        = $this->obtenerServiciosRequest($serviciosRequest);
            $montoCuenta      = $this->obtenerTotal($serviciosRequest);

            $trabajo = Trabajo::create($data);

            $trabajo->servicios()->attach($servicios);
            $trabajo->cuenta()->create([
                'monto'  => $montoCuenta,
                'estado' => 'PENDIENTE'
            ]);

            $this->code    = 200;
            $this->mensaje = 'Trabajo creado correctamente';
        } catch (\Throwable $th) {
            $this->code    = 500;
            $this->mensaje = $th->getMessage();
        }

        return api_response([], $this->mensaje, $this->code);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $data       = [];
        $relaciones = [
            'clienteProveedor:id,nombre',
            'cuenta.abonos',
            'servicios.parteMotor',
            'marca:id,nombre',
            'submarca:id,marca_id,nombre',
            'modelo:id,anio'
        ];

        try {
            $data = Trabajo::with($relaciones)->findOrFail($id);

            $this->code    = 200;
            $this->mensaje = '';
        } catch (\Throwable $th) {
            $this->code    = 404;
            $this->mensaje = $th->getMessage();
        }

        return api_response($data, $this->mensaje, $this->code);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(TrabajoRequest $request, string $id)
    {
        try {
            $data             = $request->validated();
            $serviciosRequest = $request->input('servicios');
            $servicios        = $this->obtenerServiciosRequest($serviciosRequest);
            $montoCuenta      = $this->obtenerTotal($serviciosRequest);

            $trabajo = Trabajo::findOrFail($id);

            $trabajo->update($data);
            $trabajo->servicios()->sync($servicios);
            $trabajo->cuenta()->update([
                'monto'  => $montoCuenta,
            ]);

            $this->code    = 200;
            $this->mensaje = 'Trabajo actualizado correctamente';
        } catch (\Throwable $th) {
            $this->code    = 500;
            $this->mensaje = $th->getMessage();
        }

        return api_response([], $this->mensaje, $this->code);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        try {
            $trabajo = Trabajo::findOrFail($id);
            $trabajo->update([
                'activo' => !$trabajo->activo
            ]);

            $this->code    = 200;
            $this->mensaje = ($trabajo->activo)
            ? 'Trabajo inactivado correctamente'
            : 'Trabajo activado correctamente';
        } catch (\Throwable $th) {
            $this->code    = 500;
            $this->mensaje = $th->getMessage();
        }

        return api_response([], $this->mensaje, $this->code);
    }

    private function obtenerServiciosRequest(array $servicios)
    {
        $serviciosTransformados = [];

        foreach ($servicios as $servicio) {
            $servicioId = $servicio['id'];

            $serviciosTransformados[$servicioId] = [
                'monto'     => $servicio['monto'],
                'descuento' => $servicio['descuento']
            ];
        }

        return $serviciosTransformados;
    }

    public function obtenerTotal($servicios)
    {
        $total = collect($servicios)->map(
            fn ($servicio) => $servicio['monto'] - $servicio['descuento']
        )
        ->sum();

        return $total;
    }
}
