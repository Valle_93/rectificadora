<?php

namespace App\Http\Requests\Trabajos;

use Illuminate\Foundation\Http\FormRequest;

class TrabajoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'cliente_proveedor_id'    => 'required|exists:clientes_proveedores,id',
            'marca_id'                => 'required|exists:marcas,id',
            'submarca_id'             => 'required|exists:marcas,id',
            'modelo_id'               => 'required|exists:modelos,id',
            'servicios'               => 'array|min:1',
            'servicios.*.id' => 'required|exists:servicios,id',
            'servicios.*.monto'       => 'numeric|min:1',
            'servicio.*.descuento'    => 'numeric|min:0',
            'observaciones'           => 'nullable',
            'estado'                  => 'required|in:PENDIENTE,CONCLUIDO',
            'activo'                  => 'boolean'
        ];
    }

    public function attributes(): array
    {
        return [
            'cliente_proveedor_id' => 'cliente',
            'marca_id'             => 'marca',
            'submarca_id'          => 'submarca',
            'modelo_id'            => 'modelo'
        ];
    }
}
