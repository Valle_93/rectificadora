<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;

class RegistroRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'name'     => 'required|string',
            'email'    => 'required|email|unique:users,email',
            'rol'      => 'required|exists:roles,name',
            'password' => 'required|alpha_num|min:8|confirmed'
        ];
    }

    public function attributes(): array
    {
        return  [
            'name'     => 'nombre',
            'password' => 'contraseña'
        ];
    }
}
