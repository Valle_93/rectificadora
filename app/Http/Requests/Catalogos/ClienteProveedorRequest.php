<?php

namespace App\Http\Requests\Catalogos;

use Illuminate\Foundation\Http\FormRequest;

class ClienteProveedorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        $validacion = [
            'nombre'    => 'required',
            'direccion' => 'nullable',
            'tipo'      => 'required|in:CLIENTE,PROVEEDOR,AMBOS',
            'telefono'  => 'nullable|digits:10',
            'activo'    => 'boolean'
        ];

        switch ($this->method()) {
            case 'POST':
                $validacion['rfc']     = "nullable|alpha_num|min:12|max:13|unique:clientes_proveedores,rfc";
                $validacion['email']   = "nullable|email|unique:clientes_proveedores,email";
                $validacion['celular'] = 'required|digits:10|unique:clientes_proveedores,celular';
                break;

            case 'PUT':
                $validacion['rfc']     = "nullable|unique:clientes_proveedores,rfc,$this->id";
                $validacion['email']   = "nullable|unique:clientes_proveedores,email,$this->id";
                $validacion['celular'] = "required|digits:10|unique:clientes_proveedores,celular,$this->id";
                break;
        }

        return $validacion;
    }
}
