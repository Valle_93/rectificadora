<?php

namespace App\Http\Requests\Catalogos;

use Illuminate\Foundation\Http\FormRequest;

class ModeloRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        $validacion = ['activo' => 'boolean'];

        switch ($this->method()) {
            case 'POST':
                $validacion['anio'] = 'required|digits:4|unique:modelos,anio';
                break;

            case 'PUT':
                $validacion['anio'] = "required|digits:4|unique:modelos,anio,$this->id";
                break;
        }

        return $validacion;
    }

    public function attributes(): array
    {
        return [
            'anio' => 'año'
        ];
    }
}
