<?php

namespace App\Http\Requests\Catalogos;

use Illuminate\Foundation\Http\FormRequest;

class MarcaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        $validacion = [
            'marca_id' => 'nullable|exists:marcas,id',
            'activo'   => 'boolean'
        ];

        switch ($this->method()) {
            case 'POST':
                $validacion['nombre'] = 'required|unique:marcas,nombre';
                break;

            case 'PUT':
                $validacion['nombre'] = "required|unique:marcas,nombre,$this->id";
                break;
        }

        return $validacion;
    }
}
