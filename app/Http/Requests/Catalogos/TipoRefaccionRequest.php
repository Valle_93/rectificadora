<?php

namespace App\Http\Requests\Catalogos;

use Illuminate\Foundation\Http\FormRequest;

class TipoRefaccionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        $validacion = [
            'parte_motor_id' => 'required|exists:parte_motores,id',
            'activo'         => 'boolean'
        ];

        switch ($this->method()) {
            case 'POST':
                $validacion['nombre'] = 'required|unique:tipo_refacciones,nombre';
                break;

            case 'PUT':
                $validacion['nombre'] = "required|unique:tipo_refacciones,nombre,$this->id";
                break;
        }

        return $validacion;
    }

    public function attributes(): array
    {
        return [
            'parte_motor_id' => 'parte de motor'
        ];
    }
}
