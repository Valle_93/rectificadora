<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Abono extends Model
{
    protected $table = 'abonos';

    protected $fillable = [
        'cuenta_id',
        'monto'
    ];

    protected static function boot()
    {
        parent::boot();

        function verificarCuenta(Abono $abono) {
            $abono->load('cuenta.abonos');

            $cuenta       = $abono->cuenta;
            $totalAbonado = $cuenta->abonos->sum('monto');
            $cuentaPagada = ($cuenta->estado == 'PAGADA');

            if ($cuentaPagada && ($totalAbonado < $cuenta->monto)) {
                $cuenta->update(['estado' => 'PENDIENTE']);
            } else if (!$cuentaPagada && ($totalAbonado == $cuenta->monto)) {
                $cuenta->update(['estado' => 'PAGADA']);
            }
        }

        static::created(function ($abono) {
            verificarCuenta($abono);
        });

        static::deleted(function ($abono) {
            verificarCuenta($abono);
        });
    }

    //ACCESORES Y MUTADORES
    protected function createdAt(): Attribute
    {
        return Attribute::get(
            fn ($value) => Carbon::parse($value)->translatedFormat('j \\de F Y')
        );
    }

    //RELACIONES
    public function cuenta() : BelongsTo
    {
        return $this->belongsTo(Cuenta::class);
    }
}
