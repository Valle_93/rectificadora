<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Servicio extends Model
{
    use HasFactory;

    protected $table = 'servicios';

    protected $fillable = [
        'nombre',
        'parte_motor_id',
        'activo'
    ];

    protected $casts = [
        'activo' => 'boolean'
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
    */
    protected $appends = ['texto_estado'];

    //ACCESORES Y MUTADORES
    protected function textoEstado(): Attribute
    {
        return new Attribute(
            get: fn () => ($this->activo) ? 'ACTIVO' : 'INACTIVO'
        );
    }

    //RELACIONES
    public function parteMotor()
    {
        return $this->belongsTo(ParteMotor::class);
    }

    public function trabajos()
    {
        return $this->belongsToMany(Trabajo::class, 'trabajo_servicios');
    }

    //SCOPES
    public function scopeOfBusqueda($query, $param)
    {
        if (!empty($param)) {
            return $query->whereRelation('parteMotor','nombre', 'like', '%'. $param . '%')
                ->orWhere('nombre', 'like', '%'. $param . '%');
        }

        return $query;
    }
}
