<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Cuenta extends Model
{
    protected $table = 'cuentas';

    protected $fillable = [
        'trabajo_id',
        'monto',
        'estado'
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
    */
    protected $appends = [
        'total_abonado',
        'total_restante'
    ];

    //ACCESORES Y MUTADORES
    protected function totalAbonado(): Attribute
    {
        return Attribute::get(function() {
            $total = $this->abonos->sum('monto');

            return $total;
        });
    }

    protected function totalRestante(): Attribute
    {
        return Attribute::get(function() {
            $total = $this->monto - ($this->abonos->sum('monto'));

            return $total;
        });
    }

    //RELACIONES
    public function abonos() : HasMany
    {
        return $this->hasMany(Abono::class)->orderBy('created_at', 'desc');
    }

    public function trabajo() : BelongsTo
    {
        return $this->belongsTo(Trabajo::class);
    }

    //SCOPES
   /*  public function scopeOfBusqueda($query, $param)
    {
        if (!empty($param)) {
            return $query->whereRelation('trabajo','nombre', 'like', '%'. $param . '%')
                ->orWhere('nombre', 'like', '%'. $param . '%');
        }

        return $query;
    } */
}
