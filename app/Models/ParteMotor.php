<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Casts\Attribute;
class ParteMotor extends Model
{
    protected $table = 'parte_motores';

    protected $fillable = ['nombre','activo'];

    protected $casts = [
        'activo' => 'boolean'
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['texto_estado'];

    //ACCESORES Y MUTADORES
    protected function textoEstado(): Attribute
    {
        return new Attribute(
            get: fn () => ($this->activo) ? 'ACTIVO' : 'INACTIVO'
        );
    }

    //SCOPES
    public function scopeOfBusqueda($query, $param)
    {
        if (!empty($param)) {
            return $query->where('nombre', 'like', '%'. $param . '%');
        }

        return $query;
    }
}
