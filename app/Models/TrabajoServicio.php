<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TrabajoServicio extends Model
{
    protected $table = 'trabajo_servicios';

    protected $fillable = [
        'trabajo_id',
        'servicio_id',
        'monto',
        'descuento'
    ];

    //RELACIONES
    public function servicio()
    {
        return $this->belongsTo(Servicio::class);
    }

    public function trabajo()
    {
        return $this->belongsTo(Trabajo::class);
    }
}
