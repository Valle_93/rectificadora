<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Casts\Attribute;

class ClienteProveedor extends Model
{
    protected $table = 'clientes_proveedores';

    protected $fillable = [
        'nombre',
        'rfc',
        'email',
        'direccion',
        'celular',
        'telefono',
        'tipo',
        'activo'
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['texto_estado'];

    //ACCESORES Y MUTADORES
    protected function textoEstado(): Attribute
    {
        return new Attribute(
            get: fn () => ($this->activo) ? 'ACTIVO' : 'INACTIVO'
        );
    }

    //SCOPES
    public function scopeOfBusqueda($query, $param)
    {
        if (!empty($param)) {
            return $query->where('nombre', 'like', '%'. $param . '%')
                    ->orWhere('rfc', 'like', '%'. $param . '%');
        }

        return $query;
    }
}
