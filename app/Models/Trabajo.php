<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Casts\Attribute;

class Trabajo extends Model
{
    protected $table = 'trabajos';

    protected $fillable = [
        'cliente_proveedor_id',
        'marca_id',
        'submarca_id',
        'modelo_id',
        'observaciones',
        'estado',
        'activo'
    ];

    protected $casts = [
        'activo' => 'boolean'
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
    */
    protected $appends = [
        'descuento',
        'texto_estado',
        'subtotal',
        'total'
    ];

    //ACCESORES Y MUTADORES
    protected function createdAt(): Attribute
    {
        return Attribute::get(
            fn ($value) => Carbon::parse($value)->translatedFormat('j \\de F Y')
        );
    }

    protected function descuento(): Attribute
    {
        return Attribute::get(function() {
            $descuento = $this->servicios->map(
                fn($servicio) => $servicio->pivot->descuento
            )->sum();

            return $descuento;
        });
    }

    protected function subtotal(): Attribute
    {
        return Attribute::get(function() {
            $subtotal = $this->servicios->map(
                fn($servicio) => $servicio->pivot->monto
            )->sum();

            return $subtotal;
        });
    }

    protected function total(): Attribute
    {
        return Attribute::get(function() {
            $total = $this->servicios->map(
                fn($servicio) => $servicio->pivot->monto - $servicio->pivot->descuento
            )->sum();

            return $total;
        });
    }

    protected function textoEstado(): Attribute
    {
        return new Attribute(
            get: fn () => ($this->activo) ? 'ACTIVO' : 'INACTIVO'
        );
    }

    //RELACIONES
    public function clienteProveedor()
    {
        return $this->belongsTo(ClienteProveedor::class);
    }

    public function cuenta()
    {
        return $this->hasOne(Cuenta::class);
    }

    public function marca()
    {
        return $this->belongsTo(Marca::class);
    }

    public function modelo()
    {
        return $this->belongsTo(Modelo::class);
    }

    public function servicios()
    {
        return $this->belongsToMany(Servicio::class, 'trabajo_servicios')->withPivot('monto','descuento');
    }

    public function submarca()
    {
        return $this->belongsTo(Marca::class);
    }

    //SCOPES
    public function scopeOfBusqueda($query, $param)
    {
        if (!empty($param)) {
            return $query->whereRelation('clienteProveedor','nombre', 'like', '%'. $param . '%')
                ->orWhereRelation('marca','nombre', 'like', '%'. $param . '%')
                ->orWhereRelation('submarca','nombre', 'like', '%'. $param . '%');
        }

        return $query;
    }
}
