<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Casts\Attribute;

class Marca extends Model
{
    protected $table = 'marcas';

    protected $fillable = [
        'nombre',
        'marca_id',
        'activo'
    ];

    protected $casts = [
        'activo' => 'boolean'
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['texto_estado'];

    //ACCESORES Y MUTADORES
    protected function textoEstado(): Attribute
    {
        return new Attribute(
            get: fn () => ($this->activo) ? 'ACTIVO' : 'INACTIVO'
        );
    }

    //RELACIONES
    public function marca()
    {
        return $this->belongsTo(Marca::class);
    }

    //SCOPES
    public function scopeOfBusqueda($query, $param)
    {
        if (!empty($param)) {
            return $query->whereRelation('marca','nombre', 'like', '%'. $param . '%')
                ->orWhere('nombre', 'like', '%'. $param . '%');
        }

        return $query;
    }
}
