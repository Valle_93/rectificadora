import { createApp } from 'vue';
import { createPinia } from 'pinia';
import piniaPluginPersistedstate from 'pinia-plugin-persistedstate';

//JS BOOTSTRAP
import * as bootstrap from 'bootstrap';

//ROUTER
import router from './router';

//COMPONENTE BASE
import App from './App.vue';

//COMPONENTES
import Alerta from './components/common/Alerta.vue';
import Badge from './components/common/Badge.vue';
import Buscar from './components/common/Buscar.vue';
import Card from './components/common/Card.vue';
import ErroresFormulario from './components/common/ErroresFormulario.vue';
import Spinner from './components/common/Spinner.vue';
import SubirImagen from './components/common/SubirImagen.vue';

//PLUGINS
import Iconos from './plugins/vue-font-awesome';
import PriveVue from './plugins/prime-vue';
import VueSelect from './plugins/vue-select';
import VueNumeric from 'vue-numeric';

//INICIALIZAR APLICACIÓN
const pinia = createPinia();
const app   = createApp(App);

pinia.use(piniaPluginPersistedstate);


app.use(pinia);
app.use(Iconos);
app.use(PriveVue);
app.use(router);
app.use(VueSelect);

app.component('Alerta', Alerta);
app.component('Badge', Badge);
app.component('Buscar', Buscar);
app.component('Card', Card);
app.component('ErroresFormulario', ErroresFormulario);
app.component('Spinner', Spinner);
app.component('SubirImagen', SubirImagen);

app.mount('#app');
