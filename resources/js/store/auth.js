import api from '@/api';
import router from '@/router';
import { defineStore } from 'pinia';

const useAuthStore = defineStore('auth',{
    state: () => ({
        erroresLogin: {},
        user: null
    }),
    actions: {
        async login(usuario) {
            this.erroresLogin = {};

            try {
                await api.get('/csrf-cookie');

                let { data } = await api.post('/auth/login', usuario);

                this.user         = data.data;
                this.erroresLogin = {};

                router.push({ name: 'home' });
            } catch (error) {
                let { errors } = error.response.data

                this.erroresLogin = errors
            }
        },
        async logout() {
            try {
                await api.post('/auth/logout');

                this.user = null;

                router.push({ name: 'login' });
            } catch (error) {

            }
        },
        resetearAuth() {
            this.erroresLogin = {};
        },
        async userInfo() {
            try {
                let { data } = await api.get('/user-info');

                this.user = data.data;

                router.push({ name: 'home' });
            } catch (error) {
                this.user = null;
            }
        }
    },
    getters: {
        loggedIn: (state) => !!state.user
    },
    persist: true
})

export default useAuthStore;
