import PrimeVue from 'primevue/config';
import 'primevue/resources/themes/bootstrap4-light-blue/theme.css';

//COMPONENTES
import Accordion from 'primevue/accordion';
import AccordionTab from 'primevue/accordiontab';
import ConfirmDialog from 'primevue/confirmdialog';
import ConfirmPopup from 'primevue/confirmpopup';
import Dialog from 'primevue/dialog';
import InputNumber from 'primevue/inputnumber';
import InputSwitch from 'primevue/inputswitch';
import Paginator from 'primevue/paginator';
import Toast from 'primevue/toast';

//SERVICIOS
import ConfirmationService from 'primevue/confirmationservice';
import ToastService from 'primevue/toastservice';


export default {
    install: (app) => {
        app.use(PrimeVue);
        app.use(ConfirmationService);
        app.use(ToastService);

        app.component('accordion', Accordion);
        app.component('accordion-tab', AccordionTab);
        app.component('confirm-dialog', ConfirmDialog);
        app.component('confirm-popup', ConfirmPopup);
        app.component('modal', Dialog);
        app.component('input-number', InputNumber);
        app.component('input-switch', InputSwitch);
        app.component('paginador', Paginator);
        app.component('toast', Toast);
    }
}
