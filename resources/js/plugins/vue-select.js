import vSelect from 'vue-select';
import 'vue-select/dist/vue-select.css';

export default {
    install: (app) => {
        app.component('v-select', vSelect);
    }
}
