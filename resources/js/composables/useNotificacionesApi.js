import { ref } from 'vue';
import { useToast } from 'primevue/usetoast';

export default () => {
    const toast = useToast();

    const errores = ref({});

    const manejoErroresApi = (error, status, registro = {}) => {
        let mensaje = '';

        if (status === 422) {
            let accion  = (registro?.id) ? 'editar' : 'guardar';

            mensaje = `No se pudo ${accion} el registro`;
            errores.value = error;
        } else {
            mensaje = `${status} - ${error}`;
        }

        notificacion('error', 'Error', mensaje);
    }

    const notificacion = (tipo, titulo, descripcion, conDuracion = false) => {
        let contenido = {
            severity: tipo,
            summary: titulo,
            detail: descripcion,
        }

        if (conDuracion) {
            contenido.life = 4000;
        }

        toast.add(contenido)
    }

    return { errores,manejoErroresApi,notificacion }
}
