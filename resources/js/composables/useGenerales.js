import { ref } from 'vue';
import { useConfirm } from "primevue/useconfirm";

export default () => {
    //COMPOSABLES
    const confirmar = useConfirm();

    //DATA
    const loading = ref(false);

    const  textoBotonConfirmacion = ref('');

    const verModal = ref(false);

    //METHODS
    function colorBadgeEstado(estado) {
        return (estado) ? 'success' : 'danger';
    }

    function quitarReactividad(propiedad) {
        return ref(JSON.parse(JSON.stringify(propiedad)));
    }

    return {
        confirmar,

        loading,
        quitarReactividad,
        textoBotonConfirmacion,
        verModal,

        colorBadgeEstado
    }
}
