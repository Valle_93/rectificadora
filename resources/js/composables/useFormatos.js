import accouting from 'accounting';

export default () => {

    const formatoMoneda = (valor) => accouting.formatMoney(valor);

    const formatoNumero = (valor) => accouting.formatNumber(valor, 2);

    return { formatoMoneda, formatoNumero }
}
