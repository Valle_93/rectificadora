import api from '@/api';
import useAuthStore from '@/store/auth';

export default () => {
    const authStore = useAuthStore()

    const valuesResponse = {
        data: [],
        error: null,
        mensaje: null,
        status: null
    }

    const setValuesResponse = (dataResponse) => {
        let { data,status } = dataResponse;

        valuesResponse.data    = data.data || null;
        valuesResponse.error   = (status === 422) ? data.errors : data.error;
        valuesResponse.mensaje = data.message || null;
        valuesResponse.status  = status;

        if (status === 401 || status === 403) {
            (async () => await authStore.logout())();
        }

        return valuesResponse;
    }

    const get = async (url, parametros = {}) => {
        try {
            let data = await api.get(url, { params: { ...parametros } });

            return setValuesResponse(data);
        } catch (error) {
            let { response } = error;

            return setValuesResponse(response);
        }
    }

    const post = async (url, parametros, headers = {}) => {
        try {
            let data = await api.post(url, parametros, headers);

            return setValuesResponse(data);
        } catch (error) {
            let { response } = error;

            return setValuesResponse(response);
        }
    }

    const put = async (url, parametros, headers = {}) => {
        try {
            let data = await api.put(url, parametros, headers);

            return setValuesResponse(data);
        } catch (error) {
            let { response } = error;

            return setValuesResponse(response);
        }
    }

    const patch = async (url, parametros, headers = {}) => {
        try {
            let data = await api.patch(url, parametros);

            return setValuesResponse(data);
        } catch (error) {
            let { response } = error;

            return setValuesResponse(response);
        }
    }

    const destroy = async (url, parametros = {}) => {
        try {
            let data  = await api.delete(url, parametros);

            return setValuesResponse(data);
        } catch (error) {
            let { response } = error;

            return setValuesResponse(response);
        }
    }

    return {
        get,
        post,
        put,
        patch,
        destroy
    }
}
