import { ref } from 'vue'

export default () => {
    const loadingSearch = ref(false);

    const params = ref({
        page: 1,
        per_page: 15,
        query: ''
    });

    //Methods
    const cambioPagina = async ({ page,rows }) => {
        if (page === 0) {
            page = 1;
        } else {
            page = page + 1;
        }

        params.value.page     = page;
        params.value.per_page = rows;
        params.value.query    = '';
    }

    const existenRegistros = (data) => (data.length > 0);

    const filtrarDatos = (query) => {
        params.value.page   = 1;
        params.value.query  = query;
    }

    const mensajeRegistrosPaginados = ({from,to,total}) => {
        return `Mostrando del ${from} al ${to} de ${total} registros`;
    }

    return {
        cambioPagina,
        existenRegistros,
        filtrarDatos,
        loadingSearch,
        mensajeRegistrosPaginados,
        params
    }
}
