export default [
    {
        path: '',
        name: 'trabajos',
        component: () => import(/* webpackChunkName: "trabajos-index" */ '../views/Index.vue'),
        meta: {
            title: 'Trabajos',
            auth: true
        }
    },
    {
        path: 'nuevo',
        name: 'trabajos-nuevo',
        component: () => import(/* webpackChunkName: "trabajos-nuevo" */ '../views/Crear.vue'),
        meta: {
            title: 'Nuevo Trabajo',
            auth: true
        }
    },
    {
        path: ':id/cuenta',
        name: 'trabajos-cuentas',
        component: () => import(/* webpackChunkName: "trabajos-cuentas" */ '../views/Cuenta.vue'),
        meta: {
            title: 'Cuenta de Trabajo',
            auth: true
        }
    },
    {
        path: ':id/editar',
        name: 'trabajos-editar',
        component: () => import(/* webpackChunkName: "trabajos-editar" */ '../views/Editar.vue'),
        meta: {
            title: 'Editar Trabajo',
            auth: true
        }
    },
    {
        path: ':id/detalle',
        name: 'trabajos-detalle',
        component: () => import(/* webpackChunkName: "trabajos-detalle" */ '../views/Detalle.vue'),
        meta: {
            title: 'Detalle Trabajo',
            auth: true
        }
    }
];
