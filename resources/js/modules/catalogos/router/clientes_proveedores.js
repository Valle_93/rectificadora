export default [
    {
        path: 'clientes-proveedores',
        children: [
            {
                path: '',
                name: 'clientes-proveedores',
                component: () => import(/* webpackChunkName: "clientes-proveedores-index" */ '../views/clientes-proveedores/Index.vue'),
                meta:{
                    title: 'Clientes / Proveedores',
                    auth: true
                }
            },
            {
                path: 'clientes-proveedores/nuevo',
                name: 'clientes-proveedores-nuevo',
                component: () => import(/* webpackChunkName: "clientes-proveedores-nuevo" */ '../views/clientes-proveedores/Crear.vue'),
                meta:{
                    title: 'Nuevo Cliente / Proveedor',
                    auth: true
                }
            },
            {
                path: 'clientes-proveedores/:id/editar',
                name: 'clientes-proveedores-editar',
                component: () => import(/* webpackChunkName: "clientes-proveedores-editar" */ '../views/clientes-proveedores/Editar.vue'),
                meta:{
                    title: 'Editar Cliente / Proveedor',
                    auth: true
                }
            }
        ]
    }
]
