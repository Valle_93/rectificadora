export default [
    {
        path: 'marcas',
        name: 'marcas',
        component: () => import(/* webpackChunkName: "marcas-index" */ '../views/marcas/Index.vue'),
        meta:{
            title: 'Marcas',
            auth: true
        }
    }
];
