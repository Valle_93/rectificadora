export default [
    {
        path: 'parte-motores',
        name: 'parte-motores',
        component: () => import(/* webpackChunkName: "parte-motores-index" */ '../views/parte-motores/Index.vue'),
        meta:{
            title: 'Partes de Motor',
            auth: true
        }
    },
];
