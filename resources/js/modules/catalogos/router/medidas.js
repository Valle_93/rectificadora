export default [
    {
        path: 'medidas',
        name: 'medidas',
        component: () => import(/* webpackChunkName: "medidas-index" */ '../views/medidas/Index.vue'),
        meta:{
            title: 'Medidas',
            auth: true
        }
    }
];
