import ClientesProveedoresRouter from './clientes_proveedores';
import MarcasRouter from './marcas';
import MedidasRouter from './medidas';
import ModelosRouter from './modelos';
import ParteMotoresRouter from './parte_motores';
import ServiciosRouter from './servicios';
import TipoRefaccionesRouter from './tipo_refacciones';

export default [
    ...ClientesProveedoresRouter,
    ...MarcasRouter,
    ...MedidasRouter,
    ...ModelosRouter,
    ...ParteMotoresRouter,
    ...ServiciosRouter,
    ...TipoRefaccionesRouter
];
