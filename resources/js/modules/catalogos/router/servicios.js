export default [
    {
        path: 'servicios',
        name: 'servicios',
        component: () => import(/* webpackChunkName: "servicios-index" */ '../views/servicios/Index.vue'),
        meta:{
            title: 'Servicios',
            auth: true
        }
    },
    {
        path: 'servicios/nuevo',
        name: 'servicios-nuevo',
        component: () => import(/* webpackChunkName: "servicios-nuevo" */ '../views/servicios/Crear.vue'),
        meta:{
            title: 'Nuevo Servicio',
            auth: true
        }
    },
    {
        path: 'servicios/:id/editar',
        name: 'servicios-editar',
        component: () => import(/* webpackChunkName: "servicios-editar" */ '../views/servicios/Editar.vue'),
        meta:{
            title: 'Editar Servicio',
            auth: true
        }
    }
]
