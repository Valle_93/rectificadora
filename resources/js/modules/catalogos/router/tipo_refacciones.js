export default [
    {
        path: 'tipo-refacciones',
        name: 'tipo-refacciones',
        component: () => import(/* webpackChunkName: "tipo-refacciones-index" */ '../views/tipo-refacciones/Index.vue'),
        meta:{
            title: 'Tipos de Refacción',
            auth: true
        }
    }
];
