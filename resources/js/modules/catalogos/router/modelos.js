export default [
    {
        path: 'modelos',
        name: 'modelos',
        component: () => import(/* webpackChunkName: "modelos-index" */ '../views/modelos/Index.vue'),
        meta:{
            title: 'Modelos',
            auth: true
        }
    }
];
