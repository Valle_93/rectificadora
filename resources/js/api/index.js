import axios from 'axios';

axios.baseURL = import.meta.env.VITE_URL;

const api = axios.create({
    withCredentials: true,
    withXSRFToken: true,
    baseURL: import.meta.env.VITE_API_URL
});

api.interceptors.response.use(config => {
    return Promise.resolve(config);
}, (error) => {
    return Promise.reject(error);
});

export default api;
