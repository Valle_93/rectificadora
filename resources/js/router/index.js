import { createRouter,createWebHistory } from 'vue-router';
import useAuthStore from '@/store/auth';
import RouterCatalogos from '@/modules/catalogos/router';
import RouterTrabajos from '@/modules/trabajos/router';

const routes = [
    {
        path: '',
        redirect: '/login'
    },
    {
        path: '/catalogos',
        component: () => import(/* webpackChunkName: "catalogos" */ '@/views/Catalogos.vue'),
        children:[
            ...RouterCatalogos
        ]
    },
    {
        path: '/home',
        name: 'home',
        component: () => import(/* webpackChunkName: "home" */ '@/views/Home.vue'),
        meta:{
            title: 'Home',
            auth: true
        }
    },
    {
        path: '/login',
        name: 'login',
        component: () => import(/* webpackChunkName: "login" */ '@/views/Login.vue'),
        meta:{
            title: 'Login',
            auth: false
        }
    },
    {
        path: '/trabajos',
        component: () => import(/* webpackChunkName: "trabajos" */ '@/views/Trabajos.vue'),
        children:[
            ...RouterTrabajos
        ]
    }
];

const router = createRouter({
    history: createWebHistory(),
    routes
});

router.beforeEach((to, from) => {
    const authStore     = useAuthStore();
    const { title }     = to.meta;
    const authRequerido = to.matched.some(record => record.meta.auth);

    document.title = title;

    if (authRequerido && !authStore.loggedIn) {
        return { name: 'login' }
    } else if (!authRequerido && authStore.loggedIn) {
        return { name: 'home' }
    } else {
        return true
    }
});

export default router;
