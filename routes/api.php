<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\Abonos\AbonoController;
use App\Http\Controllers\Api\Auth\AuthController;
use App\Http\Controllers\Api\Catalogos\{
    ClienteProveedorController,
    MarcaController,
    MedidaController,
    ModeloController,
    ParteMotorController,
    ServicioController,
    TipoRefaccionController,
};
use App\Http\Controllers\Api\Trabajos\TrabajoController;

Route::prefix('auth')->group(function () {
    Route::post('login', [AuthController::class, 'login']);
    Route::post('logout', [AuthController::class, 'logout']);
    Route::post('registro', [AuthController::class, 'registro']);
});

Route::middleware('auth:sanctum')->group(function () {
    //ABONOS
    Route::prefix('abonos')->group(function() {
        Route::get('', [AbonoController::class, 'index']);
        Route::post('', [AbonoController::class, 'store']);
        Route::get('{id}', [AbonoController::class, 'show']);
        Route::put('{id}', [AbonoController::class, 'update']);
        Route::delete('{id}', [AbonoController::class, 'destroy']);
    });
    Route::prefix('catalogos')->group(function () {
        //CLIENTES PROVEEDORES
        Route::get('clientes-proveedores/listado', [ClienteProveedorController::class, 'listado']);
        Route::apiResource('clientes-proveedores', ClienteProveedorController::class);

        //MARCAS
        Route::get('marcas/marcas-padre', [MarcaController::class, 'marcasPadre']);
        Route::get('marcas/{id}/submarcas', [MarcaController::class, 'submarcas']);
        Route::apiResource('marcas', MarcaController::class);

        //MARCAS
        Route::apiResource('medidas', MedidaController::class);

        //MODELOS
        Route::get('modelos/listado', [ModeloController::class, 'listado']);
        Route::apiResource('modelos', ModeloController::class);

        //PARTES DE MOTOR
        Route::get('partes-motor/listado', [ParteMotorController::class, 'listado']);
        Route::apiResource('partes-motor', ParteMotorController::class);

        //SERVICIOS
        Route::get('servicios/listado', [ServicioController::class, 'listado']);
        Route::apiResource('servicios', ServicioController::class);

        //TIPOS DE REFACCIÓN
        Route::get('tipo-refacciones/listado', [TipoRefaccionController::class, 'listado']);
        Route::apiResource('tipo-refacciones', TipoRefaccionController::class);
    });

    //TRABAJOS
    Route::prefix('trabajos')->group(function() {
        Route::get('', [TrabajoController::class, 'index']);
        Route::post('', [TrabajoController::class, 'store']);
        Route::get('{id}', [TrabajoController::class, 'show']);
        Route::put('{id}', [TrabajoController::class, 'update']);
        Route::delete('{id}', [TrabajoController::class, 'update']);
    });
});

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:sanctum');
